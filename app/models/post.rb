class Post < ApplicationRecord
	validates :body, presence: true
	validates :title, presence: true
	validates :title, length: { maximum: 50 }
	validates :title, length: { minimum: 3 }
	validates :image, presence: {:message => " must be attached to the post"}
	validates :body, length: { minimum: 20 }
  	validates :body, length: { maximum: 500 }
	paginates_per 3
	belongs_to :user
	mount_uploader :image, ImageUploader
	include PgSearch
	pg_search_scope :search, against: [:title, :body]
	has_many :likes, dependent: :destroy
end
