Rails.application.routes.draw do
  devise_for :users
  resources :posts do
  	resources :likes
  end
  resources :users
  #get 'posts', to: 'posts#index', as: :posts
  resources :search, only: [:index]
  root 'posts#index'


end
